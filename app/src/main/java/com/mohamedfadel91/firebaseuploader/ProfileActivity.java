package com.mohamedfadel91.firebaseuploader;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {

    private User post;
    private ImageView mProfilePic;
    private TextView mProfileTitle;
    private TextView mProfiledesc;
    private TextView txtIcon1;
    private TextView txtIcon2;
    private TextView postsTxt;
    private TextView followersTxt;
    private TextView followingTxt;
    private TextView followingNum;
    private TextView followersNum;
    private TextView postsNum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        String uid = auth.getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users").child(uid);

        mProfilePic = (ImageView)findViewById(R.id.profile_pic);
        mProfileTitle = (TextView)findViewById(R.id.profile_title);
        mProfiledesc = (TextView)findViewById(R.id.profile_desc);

// Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(User.class);

                Picasso.with(ProfileActivity.this).load(post.profilePicLink)
                        .into(mProfilePic);

                mProfileTitle.setText(post.title);
                mProfiledesc.setText(post.site_url);

                postsNum.setText(dataSnapshot.child("posts").child("nums").getValue() + "");
                followersNum.setText(dataSnapshot.child("followers").child("nums").getValue() + "");
                followingNum.setText(dataSnapshot.child("following").child("nums").getValue() + "");


            }
//
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
//
init_view();


    }

    private void init_view() {

        RelativeLayout posts = (RelativeLayout)findViewById(R.id.profile_posts);
        postsTxt = (TextView)posts.findViewById(R.id.item_desc);
        postsTxt.setText(getString(R.string.posts));
        postsNum = (TextView)posts.findViewById(R.id.item_number);
//
        RelativeLayout followers = (RelativeLayout)findViewById(R.id.profile_followers);
        followersTxt = (TextView)followers.findViewById(R.id.item_desc);
        followersTxt.setText(getString(R.string.followers));
        followersNum = (TextView)followers.findViewById(R.id.item_number);

//
        RelativeLayout following = (RelativeLayout)findViewById(R.id.profile_following);
        followingTxt = (TextView)following.findViewById(R.id.item_desc);
        followingTxt.setText(getString(R.string.following));
        followingNum = (TextView)following.findViewById(R.id.item_number);

//
//
        txtIcon1 = (TextView)findViewById(R.id.profile_icon1);
        txtIcon1.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/fontawesome.ttf"));
//
        txtIcon2 = (TextView)findViewById(R.id.profile_icon2);
        txtIcon2.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/fontawesome.ttf"));
//
        txtIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtIcon1.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                txtIcon2.setTextColor(getResources().getColor(android.R.color.darker_gray));
            }
        });
//
        txtIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtIcon1.setTextColor(getResources().getColor(android.R.color.darker_gray));
                txtIcon2.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
            }
        });

        Button edit = (Button)findViewById(R.id.button2);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this,EditProfileActivity.class);
                intent.setAction("profile.edit");
                startActivity(intent);
            }
        });
//
//        gridview = (GridView)findViewById(R.id.gridview);
//        String [] mlinks ={
//                "http://r.ddmcdn.com/s_f/o_1/cx_633/cy_0/cw_1725/ch_1725/w_720/APL/uploads/2014/11/too-cute-doggone-it-video-playlist.jpg",
//                "http://r.ddmcdn.com/s_f/o_1/cx_633/cy_0/cw_1725/ch_1725/w_720/APL/uploads/2014/11/too-cute-doggone-it-video-playlist.jpg",
//                "http://r.ddmcdn.com/s_f/o_1/cx_633/cy_0/cw_1725/ch_1725/w_720/APL/uploads/2014/11/too-cute-doggone-it-video-playlist.jpg",
//                "http://r.ddmcdn.com/s_f/o_1/cx_633/cy_0/cw_1725/ch_1725/w_720/APL/uploads/2014/11/too-cute-doggone-it-video-playlist.jpg"
//        };
//            gridview.setAdapter(new ImageAdapter(this,mlinks));

    }
}
