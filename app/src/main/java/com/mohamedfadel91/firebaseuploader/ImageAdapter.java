package com.mohamedfadel91.firebaseuploader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Buffon on 10/15/2016.
 */

public class ImageAdapter extends BaseAdapter {

    private final Context mContext;
    private final String[] mLinks;

    public ImageAdapter(Context context, String[] links){
        mContext = context;
        mLinks = links;
    }

    @Override
    public int getCount() {
        return mLinks.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SquareImageView picture;

        if (convertView == null) {
            convertView = new SquareImageView(mContext);
            convertView.setTag(position);
        }

        picture = (SquareImageView) convertView;
        Picasso.with(mContext).load(mLinks[position])
                .into(picture);
        return picture;
    }
}
