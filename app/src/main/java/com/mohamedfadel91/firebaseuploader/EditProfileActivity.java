package com.mohamedfadel91.firebaseuploader;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

public class EditProfileActivity extends AppCompatActivity {

    private ImageView mImageView;
    private StorageReference reference;
    private StorageReference mountainImagesRef;
    private EditText mName;
    private EditText age;
    private EditText mTitle;
    private EditText site;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

//        هذا عشان نجيب الرقم حق اليوزر
        auth = FirebaseAuth.getInstance();

        mName = (EditText)findViewById(R.id.profile_name);
        age = (EditText)findViewById(R.id.profile_age);
        mTitle = (EditText)findViewById(R.id.profile_title);
        site = (EditText)findViewById(R.id.profile_site);

//        هذا عشان اما يرجع من البروفيل
        if(getIntent().getAction().equals("profile.edit")){
            String uid = auth.getCurrentUser().getUid();

//            هذا المرجع ال هيجيب منه البيانات
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference("users").child(uid);

// Attach a listener to read the data at our posts reference
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
//                    هذا عند قراءة البيانات بنجاح
                    User post = dataSnapshot.getValue(User.class);

//                    يغير التكست باللى موجود ف قاعده البيانات
                    mTitle.setText(post.title);
                    site.setText(post.site_url);
                    age.setText(post.age + "");
                    mName.setText(post.name);
                }
                //
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
//        وهذا عشان المكان ال هنحفظ فيه الصورة البروفيل
        FirebaseStorage storage = FirebaseStorage.getInstance();

//        هذا المرجع الكامل للمساحة
        StorageReference storageRef = storage.getReferenceFromUrl("gs://fir-bb2d2.appspot.com");

//        وهذا اسم الصورة لما تتخزن ومكانها
        mountainImagesRef = storageRef.child("images/mountains.jpg");

//        طبعا هذا انت عاروفه
        Button takePicture = (Button)findViewById(R.id.profile_pic_camera);
        Button save = (Button)findViewById(R.id.button_save);
        mImageView = (ImageView)findViewById(R.id.profile_preview);
//
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                هنا حين يضغط يفتح الكاميرا كى يجيب الصورة منها
                showCamera();
            }
        });
        final Button getGallery = (Button)findViewById(R.id.profile_pic_gallery);
        getGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                هنا حين يضغط تفتح الاستوديو يجيب الصورة
                openGallery();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                هذا بياخد الصورة من الايمج فيو ال موجود ويحطها في مصفوفة
                mImageView.setDrawingCacheEnabled(true);
                mImageView.buildDrawingCache();
                Bitmap bitmap = mImageView.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

//                وهذا يرسل المصفوفة للسيرفر
                UploadTask uploadTask = mountainImagesRef.putBytes(data);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
//                         Handle unsuccessful uploads
//                        ف حالة الفشل
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                        ف حالة نجاح رفع الصورة
//                         taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
//                        هذا عشان يجيب اللينك حق الصورة ال رفعناها
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
//
//                        وهذا بيجيب البيانات اللى المستخدم دخلها ف المربعات
                        int _age = Integer.parseInt(age.getText().toString());
                        String name = mName.getText().toString();
                        String _title = mTitle.getText().toString();
                        String _site = site.getText().toString();

//                        هذا بنحطهم هنا عشان نبعتهم البيانات ال دخلها المستخدم و رابط الصورة
                        User user = new User(_age,name,_title,_site,downloadUrl.toString());
                        FirebaseDatabase database = FirebaseDatabase.getInstance();

//                        نبعتها ف قاعده البيانات تحت رقم اليوزر
                        DatabaseReference ref = database.getReference("users").child(auth.getCurrentUser().getUid());
                        ref.setValue(user);

//                        هذا عشان يعمل المنشورات والمتابعين والمتابعين بصفر ف البداية
                            user.init_infos(ref);

//

//                        هذا عشان يروح ع البروفيل
                        Intent intent = new Intent(EditProfileActivity.this,ProfileActivity.class);
                        startActivity(intent);

                    }
                });
            }
        });

    }

    private void showCamera() {

        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);
    }

    private void openGallery(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Bitmap photo = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    mImageView.setImageBitmap(photo);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    mImageView.setImageURI(selectedImage);
                }
                break;
        }
    }

}
