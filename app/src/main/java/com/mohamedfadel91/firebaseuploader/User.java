package com.mohamedfadel91.firebaseuploader;

import com.google.firebase.database.DatabaseReference;

/**
 * Created by Buffon on 10/14/2016.
 */

public class User {
    public String name;
    public String title;
    public int age;
    public String site_url;
    public String profilePicLink;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(int age, String name, String title, String site_url, String profilePicLink) {
        this.age = age;
        this.name = name;
        this.title = title;
        this.site_url = site_url;
        this.profilePicLink = profilePicLink;
    }

    public void init_infos(DatabaseReference ref){
        ref.child("posts").child("nums").setValue(0);
        ref.child("followers").child("nums").setValue(0);
        ref.child("following").child("nums").setValue(0);
    }
}
